from django.shortcuts import render, redirect
from Api import models
from django.http import HttpResponse, JsonResponse
from Api.controller import ApiCtrl

# Create your views here.

def loadData(request):

    if request.method == 'GET':
        

        print('\nInciando extracción de la data ...\n')
        data = ApiCtrl.extractData()
        print('\nActualización finalizada con exito\n')

        return JsonResponse({'status': 'success','data': data})
