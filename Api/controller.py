from Api import models
import xlrd
import datetime

class ApiCtrl:

    def extractData():

        file = '/code/data/db-data/modelo-migracion.xlsx'

        wb = xlrd.open_workbook(file)

        gruposBiologicos = []
        sh = wb.sheet_by_name('GrupoBiologico')
        gruposBiologicos = ApiCtrl.extractGruposBiologicos(sh)

        geografias = []
        sh = wb.sheet_by_name('Geografia')
        geografias = ApiCtrl.extractGeografias(sh)

        vistasGeografias= []
        sh = wb.sheet_by_name('VistaGeografica')
        vistasGeografias= ApiCtrl.extractVistasGeografias(sh)

        gruposBiologicosGeografias = []
        sh = wb.sheet_by_name('GrupoBiologicoGeografia')
        gruposBiologicosGeografias = ApiCtrl.extractGruposBiologicosGeografias(sh)

        vistasGruposBiologicos = []
        sh = wb.sheet_by_name('VistaGrupoBiologico')
        vistasGruposBiologicos = ApiCtrl.extractVistasGruposBiologicos(sh)

        entidadesPublicadoras = []
        sh = wb.sheet_by_name('EntidadPublicadora')
        entidadesPublicadoras = ApiCtrl.extractEntidadesPublicadoras(sh)

        entidadesPublicadorasGeografias = []
        sh = wb.sheet_by_name('EntidadPublicadoraGeografia')
        entidadesPublicadorasGeografias = ApiCtrl.extractEntidadesPublicadorasGeografias(sh)

        vistasEntidadesPublicadoras= []
        sh = wb.sheet_by_name('VistaEntidadPublicadora')
        vistasEntidadesPublicadoras= ApiCtrl.extractVistasEntidadesPublicadoras(sh)

        return {'gruposBiologicos': gruposBiologicos, 'geografias': geografias, 'vistasGeografias': vistasGeografias, 'gruposBiologicosGeografias': gruposBiologicosGeografias, 'vistasGruposBiologicos': vistasGruposBiologicos,'entidadesPublicadoras': entidadesPublicadoras,'entidadesPublicadorasGeografias': entidadesPublicadorasGeografias,'vistasEntidadesPublicadoras': vistasEntidadesPublicadoras}

    def extractGruposBiologicos(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        grupoBiologicoIds = []

        errors = []

        models.GrupoBiologico.objects.all().delete()
        print('Se eliminaron los datos para GrupoBiologico')

        for row in rows:
            try:
                grupoBiologico = models.GrupoBiologico.objects.create(
                    id = int(row[0]),
                    padre_id = int(row[1]),
                    nombre = str(row[2]),
                    tipo = str(row[3]),
                    aplica = True if int(row[4]) == 1 else False,
                    fechaModificacion = row[6],
                    urlImagen = row[5]
                )
                grupoBiologicoIds.append(grupoBiologico.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': grupoBiologicoIds, 'len': len(grupoBiologicoIds), 'errors': errors}

    def extractGeografias(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        geografiaIds = []

        errors = []

        models.Geografia.objects.all().delete()
        print('Se eliminaron los datos para Geografia')

        for row in rows:
            try:
                geografia = models.Geografia.objects.create(
                    id = int(row[1]),
                    padre_id = int(row[2]) if row[2] != '' else None,
                    divipola = int(row[3]),
                    padreDivipola = int(row[4]) if row[4] != '' else None,
                    nombre = row[0],
                    tipo = row[5],
                    fechaModificacion = row[6],
                )
                geografiaIds.append(geografia.id)
            except Exception as e:
                errors.append('Id '+ str(row[1]) + ' ' + str(e))

        return {'ids': geografiaIds, 'len': len(geografiaIds), 'errors': errors}

    def extractVistasGeografias(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        vistaGeografiaIds = []

        errors = []

        models.VistaGeografia.objects.all().delete()
        print('Se eliminaron los datos para VistaGeografia')

        for row in rows:
            try:
                vistaGeografia = models.VistaGeografia.objects.create(
                    id = int(row[0]),
                    geografia_id = int(row[1]),
                    especies = int(row[3]),
                    registros = int(row[4]),
                    especiesAmenaza = int(row[5]),
                    especiesVU = int(row[6]),
                    especiesEN = int(row[7]),
                    especiesCR = int(row[8]),
                    registrosAmenaza = int(row[9]),
                    registrosVU = int(row[10]),
                    registrosEN = int(row[11]),
                    registrosCR = int(row[12]),
                    especiesCites = int(row[13]),
                    especiesCitesI = int(row[14]),
                    especiesCitesII = int(row[15]),
                    especiesCitesIII = int(row[16]),
                    registrosCites = int(row[17]),
                    registrosCitesI = int(row[18]),
                    registrosCitesII = int(row[19]),
                    registrosCitesIII = int(row[20]),
                    especiesEndemicas = int(row[21]),
                    especiesExoticas = int(row[22]),
                    especiesMigratorias = int(row[23]),
                    registrosEndemicas = int(row[24]),
                    registrosExoticas = int(row[25]),
                    registrosMigratorias = int(row[26]),
                    tipoReporte = row[27],
                    fechaModificacion = row[28],
                )
                vistaGeografiaIds.append(vistaGeografia.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': vistaGeografiaIds, 'len': len(vistaGeografiaIds), 'errors': errors}

    def extractGruposBiologicosGeografias(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        gbGeografiasIds= []

        errors = []

        models.GrupoBiologicoGeografia.objects.all().delete()
        print('Se eliminaron los datos para GrupoBiologicoGeografia')

        for row in rows:
            try:
                gbGeografia = models.GrupoBiologicoGeografia.objects.create(
                    id = int(row[0]),
                    grupoBiologico_id = int(row[1]),
                    geografia_id = int(row[3]),
                    aplica = True if row[5] == '1' else False,
                    fechaModificacion =  row[6],
                )
                gbGeografiasIds.append(gbGeografia.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': gbGeografiasIds, 'len': len(gbGeografiasIds), 'errors': errors}

    def extractVistasGruposBiologicos(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        vistasGruposBiologicosIds = []

        errors = []

        models.VistaGrupoBiologico.objects.all().delete()
        print('Se eliminaron los datos para VistaGrupoBiologico')

        for row in rows:
            try:
                vistaGrupoBiologico = models.VistaGrupoBiologico.objects.create(
                    id = int(row[0]),
                    grupoBiologicoGeografia_id = int(row[1]),
                    especiesEstimadasPais = int(row[4]),
                    especies = int(row[5]),
                    registros = int(row[6]),
                    especiesAmenaza = int(row[7]),
                    especiesVU = int(row[8]),
                    especiesEN = int(row[9]),
                    especiesCR = int(row[10]),
                    registrosAmenaza = int(row[11]),
                    registrosVU = int(row[12]),
                    registrosEN = int(row[13]),
                    registrosCR = int(row[14]),
                    especiesCites = int(row[15]),
                    especiesCitesI = int(row[16]),
                    especiesCitesII = int(row[17]),
                    especiesCitesIII = int(row[18]),
                    registrosCites = int(row[19]),
                    registrosCitesI = int(row[20]),
                    registrosCitesII = int(row[21]),
                    registrosCitesIII = int(row[22]),
                    especiesEndemicas = int(row[23]),
                    especiesExoticas = int(row[24]),
                    especiesMigratorias = int(row[25]),
                    registrosEndemicas = int(row[26]),
                    registrosExoticas = int(row[27]),
                    registrosMigratorias = int(row[28]),
                    tipoReporte = row[29],
                    fechaModificacion = row[30],
                )
                vistasGruposBiologicosIds.append(vistaGrupoBiologico.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': vistasGruposBiologicosIds, 'len': len(vistasGruposBiologicosIds), 'errors': errors}

    def extractEntidadesPublicadoras(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        entidadesPublicadorasIds = []

        errors = []

        models.EntidadPublicadora.objects.all().delete()
        print('Se eliminaron los datos para EntidadPublicadora')

        for row in rows:
            try:
                entidadPublicadora = models.EntidadPublicadora.objects.create(
                    id = int(row[0]),
                    organizationId = row[1],
                    geografiaOrigen_id = int(row[3]) if row[3] != '' else None,
                    organizationCountry = row[5],
                    nombre = row[2],
                    tipo = row[4],
                    urlImagen = row[6],
                    fechaModificacion = row[7],
                )
                entidadesPublicadorasIds.append(entidadPublicadora.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': entidadesPublicadorasIds, 'len': len(entidadesPublicadorasIds), 'errors': errors}

    def extractEntidadesPublicadorasGeografias(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        entidadesPublicadorasGeografiasIds = []

        errors = []

        models.EntidadPublicadoraGeografia.objects.all().delete()
        print('Se eliminaron los datos para EntidadPublicadoraGeografia')

        for row in rows:
            try:
                entidadPublicadoraGeografia = models.EntidadPublicadoraGeografia.objects.create(
                    id = int(row[0]),
                    entidadPublicadora_id = int(row[1]),
                    geografia_id = int(row[2]),
                    aplica = True if row[3] == '1' else False,
                    fechaModificacion = row[4],
                )
                entidadesPublicadorasGeografiasIds.append(entidadPublicadoraGeografia.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': entidadesPublicadorasGeografiasIds, 'len': len(entidadesPublicadorasGeografiasIds), 'errors': errors}

    def extractVistasEntidadesPublicadoras(sh):

        rows = []

        for rx in range(sh.nrows):
            row = []
            if rx > 0:
                i = 1
                for column in sh.row(rx):
                    row.append(column.value)
                    i += 1

                rows.append(row)

        vistasEntidadesPublicadorasIds = []

        errors = []

        models.VistaEntidadPublicadora.objects.all().delete()
        print('Se eliminaron los datos para VistaEntidadPublicadora')

        for row in rows:
            try:
                vistaEntidadPublicadora = models.VistaEntidadPublicadora.objects.create(
                    id = int(row[0]),
                    entidadPublicadoraGeografia_id = int(row[1]),
                    registros = int(row[3]),
                    especies = int(row[4]),
                    fechaModificacion = row[5],
                )
                vistasEntidadesPublicadorasIds.append(vistaEntidadPublicadora.id)
            except Exception as e:
                errors.append('Id '+ str(row[0]) + ' ' + str(e))

        return {'ids': vistasEntidadesPublicadorasIds, 'len': len(vistasEntidadesPublicadorasIds), 'errors': errors}
