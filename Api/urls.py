from django.urls import path, include
from Api import views

urlpatterns = [
    path('load-data', views.loadData),
]
